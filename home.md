Welcome to the **ClearURLs** wiki!

Here we are trying to explain the operation and functionality of **ClearURLs**. If you cannot find an answer to a question about operation or functionality, please write us a note in the issues.

Please have also a look at our [FAQ](https://gitlab.com/KevinRoebert/ClearUrls/-/wikis/FAQ) page.