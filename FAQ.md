# FAQ

<p>
<details>
<summary><b>I have suggestions or complaints. Where can I submit them?</b></summary>
<br>
If you have any suggestions or complaints, please [create an issue](https://gitlab.com/KevinRoebert/ClearUrls/issues/new).
</details>
</p>

<p>
<details>
<summary><b>Before a clean URL was visible, a dirty URL was briefly in the URL bar. Does ClearURLs work correctly?</b></summary>
<br>
Sometimes a dirty URL appears first in the URL bar before a clean URL is displayed. This does not mean that the dirty URL has actually been called.<br>
<br>
ClearURLs checks all requests made through the browser, whether initiated by a user or by a background process. Each request passes through ClearURLs' filter, which is incremental, meaning that sometimes a URL that is not yet clean is passed back to the browser. This URL passes through the filter of ClearURLs again before the actual call and is then further cleaned in the next step. This happens until ClearURLs makes no more changes to the URL, then the URL passes through the filter of ClearURLs. It is therefore possible that a URL that is not clean may temporarily appear in the URL bar.<br>
<br>
To make sure that the URL is indeed cleaned correctly, you can use the integrated Cleaning Tool. Here you can clean the resulting URL again and again until no more changes occur, then you have the final clean URL.
</details>
</p>

<p>
<details>
<summary><b>How can I quickly check if ClearURLs works correctly?</b></summary>
<br>
You can visit the test page (https://kevinroebert.gitlab.io/ClearUrls/) of ClearURLs. If only shines on the page green emojis, ClearURLs works correctly. Otherwise there is an error.<br>
<br>
Please do not forget to activate JavaScript for this test page.
</details>
</p>

<p>
<details>
<summary><b>Why does ClearURLs require permission X?</b></summary>
<br>
A discussion thread about why ClearURLs requires certain permissions can be found here: https://gitlab.com/KevinRoebert/ClearUrls/issues/159
</details>
</p>

<p>
<details>
<summary><b>For which browsers are ClearURLs available?</b></summary>
<br>
<a href="https://addons.mozilla.org/en-US/firefox/addon/clearurls/"><img src="https://blog.mozilla.org/addons/files/2020/04/get-the-addon-fx-apr-2020.svg" alt="for Firefox" height="60px"></a> <a href="https://microsoftedge.microsoft.com/addons/detail/mdkdmaickkfdekbjdoojfalpbkgaddei"><img src="https://gitlab.com/KevinRoebert/ClearUrls/-/raw/master/promotion/MEA-button.png" alt="for Edge" height="60px"></a> <a href="https://chrome.google.com/webstore/detail/clearurls/lckanjgmijmafbedllaakclkaicjfmnk"><img src="https://developer.chrome.com/webstore/images/ChromeWebStore_BadgeWBorder_v2_206x58.png" alt="for Chrome" height="60px"></a>
<br>
ClearURLs is currently officially supported by Mozilla Firefox, Google Chrome and Microsoft Edge. However, it should theoretically be possible to run the addon in all Firefox or Chrome based browsers.
</details>
</p>

<p>
<details>
<summary><b>Where do I find the packed version of the addon, e.g. for debugging?</b></summary>
<br>
Here you can download the packed files for the Firefox- and Chrome-Dev: https://gitlab.com/KevinRoebert/ClearUrls/-/jobs/artifacts/master/raw/ClearUrls.zip?job=bundle%20addon
</details>
</p>