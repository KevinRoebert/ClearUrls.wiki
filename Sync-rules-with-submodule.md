Execute this commands to get the latest rules from the submodule:

```
git pull --recurse-submodules
git submodule update --remote --recursive
```